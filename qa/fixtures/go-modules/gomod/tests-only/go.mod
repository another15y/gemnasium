module gitlab.com/gitlab-org/security-products/tests/go-modules

go 1.20

require github.com/stretchr/testify v1.8.4

require (
	github.com/klauspost/cpuid/v2 v2.2.3 // indirect
	github.com/minio/sha256-simd v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20220704084225-05e143d24a9e // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/minio/minio v0.0.0-20180419184637-5a16671f721f
	github.com/minio/minio-go v6.0.14+incompatible // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
