{
  "version": "15.0.4",
  "vulnerabilities": [
    {
      "id": "ae97c63e47ecba4d366b87d9b23e47d8c71c0ae463070acd20c27c96c4732f53",
      "name": "SQL Injection",
      "description": "Due to an error in shallow key transformation, key and index lookups for `django.contrib.postgres.fields.JSONField`, and key lookups for `django.contrib.postgres.fields.HStoreField`, were subject to SQL injection. This could, for example, be exploited via crafted use of `OR 1=1` in a key or index name to return all records, using a suitably crafted dictionary, with dictionary expansion, as the `**kwargs` passed to the `QuerySet.filter()` function.",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "value": "1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14234.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14234",
          "value": "CVE-2019-14234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14234"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14234"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "16ddf4a5ffb8d866bbdbf627652d5d57dc9c3192453796689a6f95f0acc50281",
      "name": "Weak Password Recovery Mechanism for Forgotten Password",
      "description": "Django allows account takeover. A suitably crafted email address (that is equal to an existing user's email address after case transformation of Unicode characters) would allow an attacker to be sent a password reset token for the matched user account. (One mitigation in the new releases is to send password reset tokens only to the registered user email address.)",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.27, 2.2.9, 3.0.1 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-e1db52dc-7f84-4281-8b82-6a64da18a721",
          "value": "e1db52dc-7f84-4281-8b82-6a64da18a721",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-19844.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-19844",
          "value": "CVE-2019-19844",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19844"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-19844"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/dec/18/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "c899827cb63322882d75980a5935c90b0d47e8c610e31d734c3af120328d6957",
      "name": "SQL Injection",
      "description": "Django allows SQL Injection if untrusted data is used as a delimiter (e.g., in Django applications that offer downloads of data as a series of rows with a user-specified column delimiter). By passing a suitably crafted delimiter to a `contrib.postgres.aggregates.StringAgg` instance, it was possible to break escaping and inject malicious SQL.",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.28, 2.2.10, 3.0.3 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-ea400310-29db-4c48-8236-42d462d1de1c",
          "value": "ea400310-29db-4c48-8236-42d462d1de1c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-7471.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7471",
          "value": "CVE-2020-7471",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7471"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7471"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/feb/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "ce23c88a97cf0716acb6456a11f2a66fc0e10b8d2c02f282277d3dd1acb53a0c",
      "name": "Information exposure in HTTP headers",
      "description": "The Requests package for Python sends an HTTP Authorization header to an HTTP URI upon receiving a same-hostname https-to-http redirect, which makes it easier for remote attackers to discover credentials by sniffing the network.",
      "severity": "Critical",
      "solution": "Upgrade to version 2.20.0 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-2d0dd26c-c781-40e1-b126-7f20b21de048",
          "value": "2d0dd26c-c781-40e1-b126-7f20b21de048",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2018-18074.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-18074",
          "value": "CVE-2018-18074",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-18074"
        }
      ],
      "links": [
        {
          "url": "http://cwe.mitre.org/data/definitions/255.html"
        },
        {
          "url": "http://docs.python-requests.org/en/master/community/updates/#release-and-version-history"
        },
        {
          "url": "https://bugs.debian.org/910766"
        },
        {
          "url": "https://github.com/requests/requests/commit/c45d7c49ea75133e52ab22a8e9e13173938e36ff"
        },
        {
          "url": "https://github.com/requests/requests/issues/4716"
        },
        {
          "url": "https://github.com/requests/requests/pull/4718"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-18074"
        },
        {
          "url": "https://usn.ubuntu.com/3790-1/"
        },
        {
          "url": "https://usn.ubuntu.com/3790-2/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "requests:2.5.3"
        }
      }
    },
    {
      "id": "66967dbd3b190a08a8591d04e02ee0d46efc58a181da990809a1438b963872fb",
      "name": "Improper Input Validation",
      "description": "A call to the methods `chars()` or `words() in `django.utils.text.Truncator` with the argument `html=True` evaluates certain inputs extremely slowly due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and `words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-3fee86b0-d8c1-4c65-9242-7607562190ae",
          "value": "3fee86b0-d8c1-4c65-9242-7607562190ae",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14232.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14232",
          "value": "CVE-2019-14232",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14232"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14232"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "d1772e30c0d294b26c93364952accb5016fb965083ebeed0f434fa05e13b1509",
      "name": "Denial-of-service",
      "description": "If passed certain inputs, `django.utils.encoding.uri_to_iri` could lead to significant memory usage due to a recursion when repercent-encoding invalid UTF-8 octet sequences.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "value": "49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14235.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14235",
          "value": "CVE-2019-14235",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14235"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14235"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "a8ec1d41e31dae60078cf7b51c03202874cd9da7971ab04ad7cd0f299ec70722",
      "name": "Improper Input Validation",
      "description": "Due to the behaviour of the underlying HTMLParser, `django.utils.html.strip_tags` would be extremely slow to evaluate certain inputs containing large sequences of nested incomplete HTML entities.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "value": "4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14233.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14233",
          "value": "CVE-2019-14233",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14233"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14233"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "b3e8d902463ae36ccc7baac59b287209dfbe92527ad92733332fb87356f7bbfc",
      "name": "SQL Injection",
      "description": "Django allows SQL Injection if untrusted data is used as a tolerance parameter in GIS functions and aggregates on Oracle. By passing a suitably crafted tolerance to GIS functions and aggregates on Oracle, it was possible to break escaping and inject malicious SQL.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.29, 2.2.11, 3.0.4 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "value": "5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-9402.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-9402",
          "value": "CVE-2020-9402",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9402"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-9402"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/mar/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "4acf47738b026fa55f60a8d16d498d61b70b42eb4db0d648feefabf0f20c90b5",
      "name": "Uncontrolled Memory Consumption",
      "description": "Django allows Uncontrolled Memory Consumption via a malicious attacker-supplied value to the `django.utils.numberformat.format()` function.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.20, 2.0.12, 2.1.7 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "value": "aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-6975.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-6975",
          "value": "CVE-2019-6975",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106964"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/789.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/WTwEAprR0IQ"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/66WMXHGBXD7GSM3PEXVCMCAGLMQYHZCU/"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-6975"
        },
        {
          "url": "https://usn.ubuntu.com/3890-1/"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/feb/11/security-releases/"
        },
        {
          "url": "https://www.openwall.com/lists/oss-security/2019/02/11/1"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "cdb380a99481bfdf8a33bdf2efad1676c7d7218cd5d7bd80a1ae9027cfb652bd",
      "name": "Cross-site Scripting",
      "description": "An issue was discovered in Django. The `clickable` Current URL value displayed by the `AdminURLFieldWidget` displays the provided value without validating it as a safe URL. Thus, an unvalidated value stored in the database, or a value provided as a URL query parameter payload, could result in a clickable JavaScript link.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.21, 2.1.9, 2.2.2 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "value": "149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12308.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12308",
          "value": "CVE-2019-12308",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12308"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/1.11.21/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.1.9/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.2.2/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://github.com/django/django/commit/09186a13d975de6d049f8b3e05484f66b01ece62"
        },
        {
          "url": "https://github.com/django/django/commit/afddabf8428ddc89a332f7a78d0d21eaf2b5a673"
        },
        {
          "url": "https://github.com/django/django/commit/c238701859a52d584f349cce15d56c8e8137c52b"
        },
        {
          "url": "https://github.com/django/django/commit/deeba6d92006999fee9adfbd8be79bf0a59e8008"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/GEbHU7YoVz8"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12308"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jun/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "de3ed8b1780541476a2ed6e3fb419018a5b7cb635ba81012c7dc0dfcbad2420f",
      "name": "Incorrect Regular Expression",
      "description": "If `django.utils.text.Truncator`'s `chars()` and `words()` methods were passed the `html=True` argument, they were extremely slow to evaluate certain inputs due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "value": "1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7537.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7537",
          "value": "CVE-2018-7537",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7537"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103357"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7537"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "6c11dbd4fb43f774c0e71cbc23676cd005239ac1a94a29d3bc765212ea8def92",
      "name": "URL Redirection to Untrusted Site (Open Redirect)",
      "description": "`django.middleware.common.CommonMiddleware` has an Open Redirect.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.15, 2.0.8 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-25669e1a-dcaa-4722-adfc-0f089945c95c",
          "value": "25669e1a-dcaa-4722-adfc-0f089945c95c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-14574.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-14574",
          "value": "CVE-2018-14574",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14574"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/104970"
        },
        {
          "url": "http://www.securitytracker.com/id/1041403"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-14574"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "f52adb959b9e654f2bd4b7b400295bfa1341794f5c5a59f1e38383e6fa07bbd4",
      "name": "Improper Input Validation",
      "description": "An HTTP request is not redirected to HTTPS when the `SECURE_PROXY_SSL_HEADER` and `SECURE_SSL_REDIRECT` settings are used, and the proxy connects to Django via HTTPS. In other words, `django.http.HttpRequest.scheme` has incorrect behavior when a client uses HTTP.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.22, 2.1.10, 2.2.3 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "value": "42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12781.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12781",
          "value": "CVE-2019-12781",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12781"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12781"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jul/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "d489fa8bf54567e20432efae3f5eec14052b16a7945a9dec106b6506979a6261",
      "name": "Incorrect Regular Expression",
      "description": "An issue was discovered in Django. The `django.utils.html.urlize()` function was extremely slow to evaluate certain inputs due to catastrophic backtracking vulnerabilities in two regular expressions. The `urlize()` function is used to implement the urlize and urlizetrunc template filters, which were thus vulnerable.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-483f1247-a680-44c4-b609-c7a3ac65bd82",
          "value": "483f1247-a680-44c4-b609-c7a3ac65bd82",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7536.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7536",
          "value": "CVE-2018-7536",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7536"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103361"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7536"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "f0f19ddac2968462c52b2d446ac725248af9ee5a4690a9831219784e9ceedd3e",
      "name": "Possible XSS in traceback section of technical 500 debug page",
      "description": "HTML auto-escaping was disabled in a portion of the template for the technical debug page. Given the right circumstances, this allowed a cross-site scripting attack. This vulnerability shouldn't affect most production sites since you shouldn't run with `DEBUG = True` (which makes this page accessible) in your production settings.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.10.8, 1.11.5 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-6162a015-8635-4a15-8d7c-dc9321db366f",
          "value": "6162a015-8635-4a15-8d7c-dc9321db366f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2017-12794.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2017-12794",
          "value": "CVE-2017-12794",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12794"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/100643"
        },
        {
          "url": "http://www.securitytracker.com/id/1039264"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2017-12794"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2017/sep/05/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "a80ce4a5612edb7216d6c83901b98ebcdc77a9d5cd89cf8cc452292b3d4d4607",
      "name": "Content Spoofing",
      "description": "Improper Neutralization of Special Elements in Output Used by a Downstream Component issue exists in `django.views.defaults.page_not_found()`, leading to content spoofing (in a 404 error page) if a user fails to recognize that a crafted URL has malicious content.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.18, 2.0.10, 2.1.5 or above.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "value": "94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-3498.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-3498",
          "value": "CVE-2019-3498",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106453"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/148.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/VYU7xQQTEPQ"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2019/01/msg00005.html"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-3498"
        },
        {
          "url": "https://usn.ubuntu.com/3851-1/"
        },
        {
          "url": "https://www.debian.org/security/2019/dsa-4363"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jan/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "8e6d111559d3cd555aa0173e05a0f862aa9000e78109a6c2549b76c26f07f2bf",
      "name": "Session fixation in resolve_redirects()",
      "description": "The `resolve_redirects()` function in `sessions.py` allows a remote, user-assisted attacker to conduct a session fixation attack. This flaw exists because the application, when establishing a new session, does not invalidate an existing session identifier and assign a new one. With a specially crafted request fixating the session identifier, a context-dependent attacker can ensure a user authenticates with the known session identifier, allowing the session to be subsequently hijacked.",
      "severity": "Medium",
      "solution": "Upgrade to latest version.",
      "location": {
        "file": "setup.py",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "value": "d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2015-2296.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2015-2296",
          "value": "CVE-2015-2296",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-2296"
        }
      ],
      "links": [
        {
          "url": "http://osvdb.org/show/osvdb/119576"
        },
        {
          "url": "http://www.openwall.com/lists/oss-security/2015/03/14/4"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/3bd8afbff29e50b38f889b2f688785a669b9aafc#diff-28e67177469c0d36b068d68d9f6043bf"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/f7c85685a8e484715649c13bacae6adc7f5f3908#diff-28e67177469c0d36b068d68d9f6043bf"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "requests:2.5.3"
        }
      }
    }
  ],
  "dependency_files": [
    {
      "path": "setup.py",
      "package_manager": "setuptools",
      "dependencies": [
        {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        },
        {
          "package": {
            "name": "docutils"
          },
          "version": "0.13.1"
        },
        {
          "package": {
            "name": "pytz"
          },
          "version": ":SKIP:"
        },
        {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        },
        {
          "package": {
            "name": "test-project"
          },
          "version": "0.0.1"
        }
      ]
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.6.0"
    },
    "scanner": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.20.1"
    },
    "type": "dependency_scanning",
    "start_time": "2022-03-03T06:59:10",
    "end_time": "2022-03-03T06:59:17",
    "status": "success"
  }
}
