libraryDependencies ++= {
  sbtVersion.value match {
    case version if version startsWith "1.9.7" =>
      Seq(
        // The compiler-bridge is a tool that sits between sbt and the Scala
        // compiler. It lets sbt work with any Scala version by adjusting itself
        // based on the project's Scala version.
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.9.5", // main artifact
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.9.5" classifier "sources", // sources jar
        "org.scala-sbt" % "sbt-dependency-tree_2.12_1.0" % "1.9.7"
      )
    case _ =>
      Seq("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")
  }
}
